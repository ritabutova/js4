'use strict';

let a = prompt('Введіть перше число'); 
while (isNaN(a) || a === '') {
    a = prompt('Введіть перше ЧИСЛО', a);
}
let b = prompt('Введіть друге число');
while (isNaN(b) || b === '') {
    b = prompt('Введіть друге ЧИСЛО', b);
}
let c = prompt('Яку дію ви бажаєте виконати + - * / ?');
while (c !== '+' && c !== '-' && c!== '*' && c !== '/') {
    c = prompt('Введіть один із варіантів: + - * /');
}

function calc(x, y, z) {
    if (z === '+') {
        return +x + +y;
    }
    else if (z === '-') {
        return x - y;
    }
    else if (z === '*') {
        return x * y;
    }
    else if (z === '/') {
        return x / y;
    }
}

let result = calc(a, b, c);
console.log(result);